package com.erika;

import com.erika.logic.GeneticAlgorithm;
import com.erika.model.Population;

import java.util.Scanner;

/**
 * Erika Repasska | 3. zadanie | Geneticky algoritmus na hladanie pokladov
 * Umela inteligencia | FIIT STU BA | Letny semester 2017/2018
 **/

public class Main {

    public static void main(final String[] args) {

        Population population = new Population();
        final Scanner scanner = new Scanner(System.in);

        System.out.println("Choose size of population: ");
        final int populationSize = scanner.nextInt();

        System.out.println("Choose mutation rate: ");
        final double mutationRate = scanner.nextDouble();

        System.out.println("Elitism? [true | false]: ");
        final boolean elitism = scanner.nextBoolean();

        System.out.println("Choose type of selection [1 - tournament | 2 - roulette]: ");
        final int selectionType = scanner.nextInt();

        System.out.println("Choose number of generations: ");
        final int numberOfGenerations = scanner.nextInt();

        //vygenerovanie prvej populacie
        population.createFirstGenerationHunters(populationSize);
        population = GeneticAlgorithm.testPopulation(population, mutationRate, elitism, selectionType);

        int generationCount = 1;
        //generovanie novych populacii, kym nie je najdene riesenie
        while (population.getFittest().getNumberOfFoundTreasures() < 5) {
            System.out.println("Generation: " + generationCount + " Fittest: " + population.getFittest().getFitnessValue());
            population = GeneticAlgorithm.evolvePopulation(population);
            population = GeneticAlgorithm.testPopulation(population, mutationRate, elitism, selectionType);
            generationCount++;
            if (generationCount % numberOfGenerations == 0) {
                System.out.println("Want to continue? ['true' for YES | 'false' for NO]");
                final boolean wantToContinue = scanner.nextBoolean();
                if (!wantToContinue) {
                    System.out.println("Solution not found! :(");
                    return;
                }
            }
        }
        System.out.println("Solution found!");
        System.out.println("Generation: " + generationCount);
        System.out.println("Fitness value:");
        System.out.println(population.getFittest().getFitnessValue());
        System.out.println("Treasures:");
        System.out.println(population.getFittest().getNumberOfFoundTreasures());
        System.out.println("Path: [" + population.getFittest().getCrossedPath().size() + " steps]" );
        int index = 1;
        for (final Character direction : population.getFittest().getCrossedPath()) {
            System.out.println(index + ". " + direction);
            index++;
        }
    }
}

