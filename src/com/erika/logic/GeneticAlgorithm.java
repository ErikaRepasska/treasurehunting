package com.erika.logic;

import com.erika.model.Hunter;
import com.erika.model.Population;

import java.util.Random;

/**
 * V tejto triede sa vytvaraju nove populacie.
 * Na vytvorenie novej populacie sa pouziva krizenie jedincov v metode getResultOfCrossover().
 * Jedinci, ktori sa krizia, su vyberany bud formou turnaju alebo rulety.
 * Turnaj predstavuje metoda tournamentSelection() a ruletu rouletteSelcetion().
 * Niektori jedinci v novej populacii nahodne mutuju z zavislosti od mutationRate metodou mutate().
 * Elitism zabezpecuje bezprostredne posunutie najlepsieho jedinca do dalsej generacie.
 **/

public class GeneticAlgorithm {

    // parametre genetickeho algoritmu
    private static final double UNIFORM_RATE = 0.5;
    private static final int TOURNAMENT_SIZE = 4;
    private static boolean elitism = true;
    private static double mutationRate = 0.015;
    private static int selectionType = 1; //1 - tournament, 2 - roulette

    public static Population testPopulation(final Population population, final double chosenMutationRate, final boolean chosenElitism, final int selection) {
        int index = 0;
        mutationRate = chosenMutationRate;
        elitism = chosenElitism;
        selectionType = selection;
        for (final Hunter firstGenerationHunter : population.getHunters()) {
            //System.out.println(index + ". H U N T E R >>>");
            new VirtualMachine().start(firstGenerationHunter);
            index++;
        }

        index = 0;
        for (final Hunter firstGenerationHunter : population.getHunters()) {
            //System.out.println(index + ". H U N T E R FITNESS >>>");
            //System.out.println(firstGenerationHunter.getFitnessValue());
            index++;
        }

        return population;
    }

    public static Population evolvePopulation(final Population actualPopulation) {
        final Population newPopulation = new Population();

        if (elitism) {
            final Hunter fittestHunter = actualPopulation.getFittest();
            newPopulation.addHunter(fittestHunter);
            actualPopulation.removeHunter(fittestHunter);
        }

        for (final Hunter ignored : actualPopulation.getHunters()) {
            if (selectionType == 1) {
                final Hunter tournamentWinner1 = tournamentSelection(actualPopulation);
                final Hunter tournamentWinner2 = tournamentSelection(actualPopulation);
                newPopulation.addHunter(getResultOfCrossover(tournamentWinner1, tournamentWinner2));
            } else if (selectionType == 2) {
                final Hunter rouletteWinner1 = rouletteSelection(actualPopulation);
                final Hunter rouletteWinner2 = rouletteSelection(actualPopulation);
                newPopulation.addHunter(getResultOfCrossover(rouletteWinner1, rouletteWinner2));
            } else {
                System.out.println("INVALID SELECTION!");
                return null;
            }
        }

        for (final Hunter hunter : newPopulation.getHunters()) {
            mutate(hunter);
        }
        return newPopulation;
    }

    private static Hunter tournamentSelection(final Population actualPopulation) {
        final Population tournament = new Population();
        for (int i = 0; i < TOURNAMENT_SIZE; i++) {
            final int randomId = (int) (Math.random() * actualPopulation.getHunters().size());
            tournament.addHunter(actualPopulation.getHunters().get(randomId));
        }
        return tournament.getFittest();
    }

    private static Hunter rouletteSelection(final Population actualPopulation) {
        final double sumOfFitnessValue = actualPopulation.getSumOfFitnessValue();
        double sumOfProbabilities = 0;
        for (final Hunter hunter : actualPopulation.getHunters()) {
            final double fitnessValue = hunter.getFitnessValue() + 1000;
            final double temporaryValue = (fitnessValue / sumOfFitnessValue);
            final double hunterProbability = sumOfProbabilities + temporaryValue;
            hunter.setRouletteProbability(hunterProbability);
            sumOfProbabilities += temporaryValue;
            //System.out.println("ROULETTE -> probability *** " + hunterProbability + " ***");
        }

        for (final Hunter hunter : actualPopulation.getHunters()) {
            if (Math.random() > hunter.getRouletteProbability()) {
                //System.out.println("ROULETTE -> chosen fitness value *** " + hunter.getFitnessValue() + " ***");
                return hunter;
            }
        }
        return actualPopulation.getFittest();
    }

    private static Hunter getResultOfCrossover(final Hunter winner1, final Hunter winner2) {
        final Hunter crossedHunter = new Hunter();
        for (int i = 0; i < winner1.getGenome().length; i++) {
            if (Math.random() <= UNIFORM_RATE) {
                crossedHunter.setGeneOnIndex(i, winner1.getGeneOnIndex(i));
            } else {
                crossedHunter.setGeneOnIndex(i, winner2.getGeneOnIndex(i));
            }
        }
        return crossedHunter;
    }

    private static void mutate(final Hunter hunter) {
        final Random random = new Random();
        for (int i = 0; i < hunter.getGenome().length; i++) {
            if (Math.random() <= mutationRate) {
                hunter.setGeneOnIndex(i, random.nextInt(256));
            }
        }
    }
}
