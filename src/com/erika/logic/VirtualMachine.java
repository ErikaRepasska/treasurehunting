package com.erika.logic;

import com.erika.model.Hunter;
import com.erika.model.Position;

/**
 * Trieda reprezentujuca virtualny stroj.
 **/

public class VirtualMachine {

    private static final char START = 's';
    private static final char TREASURE = 'x';
    private static final char CELL = '0';

    //mapa s rozmiestnenymi pokladmi a startovacou poziciou
    private final char map[][] = {{CELL, CELL, CELL, CELL, CELL, CELL, CELL},
            {CELL, CELL, CELL, CELL, TREASURE, CELL, CELL},
            {CELL, CELL, TREASURE, CELL, CELL, CELL, CELL},
            {CELL, CELL, CELL, CELL, CELL, CELL, TREASURE},
            {CELL, TREASURE, CELL, CELL, CELL, CELL, CELL},
            {CELL, CELL, CELL, CELL, TREASURE, CELL, CELL},
            {CELL, CELL, CELL, START, CELL, CELL, CELL}};

    //spustenie virtualneho stroja
    public void start(final Hunter hunter) {
        final int[] copyOfGenomeArrayForModification = hunter.getCopyOfGenome();
        //final int[] moves = new int[100];
        int numberOfInc = 0;
        int numberOfDec = 0;
        int numOfJumps = 0;
        int numOfPrint = 0;

        int index = 0;
        int numberOfSteps = 0;

        //vykonavanie instrukcii na prislusnych adresach
        while (index < 64 && hunter.getNumberOfFoundTreasures() < 5 && numberOfSteps < 500) {
            //System.out.println(index + ". decimal value: " + copyOfGenomeArrayForModification[index]);
            final int binaryValue = toBinary(copyOfGenomeArrayForModification[index]);
            final int firstBits = getFirst2Bits(copyOfGenomeArrayForModification[index]); //instruction
            final int lastBits = getLast6Bits(copyOfGenomeArrayForModification[index]); //address
            //System.out.println("Binary value: " + String.valueOf(binaryValue));
            //System.out.println("First 2 bits (instruction): " + String.valueOf(toBinary(firstBits)));
            //System.out.println("Last 6 bits (address): " + String.valueOf(toBinary(lastBits)));

            if (toBinary(firstBits) == 0) { //inkrementacia
                numberOfInc++;
                //System.out.println("Number to be modified: " + copyOfGenomeArrayForModification[lastBits] + " || index: " + lastBits);
                increment(lastBits, copyOfGenomeArrayForModification);
                //System.out.println("Modification: " + copyOfGenomeArrayForModification[lastBits]);
                //System.out.println("-- INC --");
                index++;
            }
            if (toBinary(firstBits) == 1) { //dekrementacia
                numberOfDec++;
                //System.out.println("Number to be modified: " + copyOfGenomeArrayForModification[lastBits] + " || index: " + lastBits);
                decrement(lastBits, copyOfGenomeArrayForModification);
                //System.out.println("Modification: " + copyOfGenomeArrayForModification[lastBits]);
                //System.out.println("-- DEC --");
                index++;
            }
            if (toBinary(firstBits) == 10) { //skok
                numOfJumps++;
                //System.out.println("-- JUMP --");
                index = lastBits;
            }
            if (toBinary(firstBits) == 11) { //vypis (posun)
                numOfPrint++;
                //System.out.println("-- MOVE --");
                final int direction = getMove(toBinary(copyOfGenomeArrayForModification[lastBits]));
                //moves[movesIndex] = direction;
                //System.out.println("Move direction = " + direction);
                if (hunter.move(direction)) {
                    digForTreasure(hunter);
                } else {
                    break;
                }
                index++;
            }
            if (index == 64) { //ak sme dosli na poslednu bunku, ideme opat od pociatocnej bunky
                index = 0;
            }
            //System.out.println("---------------------------------------");
            numberOfSteps++;
        }

        /*System.out.println("numberOfInc " + numberOfInc);
        System.out.println("numberOfDec " + numberOfDec);
        System.out.println("numOfJumps " + numOfJumps);
        System.out.println("numOfPrint " + numOfPrint);
        System.out.println("numberOfTreasures " + hunter.getNumberOfFoundTreasures());

        for (int i = 0; i < movesIndex; i++) {
            System.out.println("Moves -> ");
            System.out.println(moves[i]);
        }*/

        hunter.setFitnessValue(getCalculatedFitness(hunter.getNumberOfFoundTreasures(), hunter.getCrossedPath().size()));
        //ToDo: Ak ratame aj krok veduci uz MIMO mapu do fitness-u, musime dat .size()+1 ! :)
    }

    //ziskanie instrukcie
    private int getFirst2Bits(final int number) {
        return number >> 6;
    }

    //ziskanie adresy
    private int getLast6Bits(final int number) {
        return number & 0x3F;
    }

    //hladanie pokladu na aktualnej pozicii
    private void digForTreasure(final Hunter hunter) {
        int currentNumberOfTreasures = hunter.getNumberOfFoundTreasures();
        if (map[hunter.getCurrentPosition().getY()][hunter.getCurrentPosition().getX()] == TREASURE) {
            currentNumberOfTreasures++;
            map[hunter.getCurrentPosition().getY()][hunter.getCurrentPosition().getX()] = CELL;
            hunter.setNumberOfFoundTreasures(currentNumberOfTreasures);
        }
    }

    //prevedenie int to binarnej hodnoty pre pracu s virtualnym strojom
    private int toBinary(final int number) {
        return Integer.valueOf(Integer.toBinaryString(number));
    }

    private void decrement(final int index, final int[] array) {
        if (array[index] == 0) {
            array[index] = 255;
        } else {
            array[index]--;
        }
    }

    private void increment(final int index, final int[] array) {
        if (array[index] == 255) {
            array[index] = 0;
        } else {
            array[index]++;
        }
    }

    private int getMove(int hunterCell) {
        int numberOfOnes = 0;
        int currentDigit;
        while (hunterCell != 0) {
            currentDigit = hunterCell % 10;
            hunterCell /= 10;

            if (currentDigit == 1)
                ++numberOfOnes;
        }
        if (numberOfOnes <= 2) return Position.UP;
        if (numberOfOnes == 3 || numberOfOnes == 4) return Position.DOWN;
        if (numberOfOnes == 5 || numberOfOnes == 6) return Position.RIGHT;
        if (numberOfOnes == 7 || numberOfOnes == 8) return Position.LEFT;

        return Position.INVALID_DIRECTION;
    }

    //vypocet fitness hodnoty
    private int getCalculatedFitness(final int treasures, final int moves) {
        int fitness = ((treasures * 1000) - moves);
        return fitness;
    }
}
