package com.erika.model;

/**
 * Tato trieda zaznamenava aktualnu poziciu na mape a jednotlive posunutia.
 * Jej metody realizuju pohyby hore, dole, dolava, doprava.
 * Metody getX a getY ziskaju aktualnu poziciu na mape.
 **/

public class Position {

    public static final int INVALID_DIRECTION = -1;
    public static final int RIGHT = 1;
    public static final int UP = 2;
    public static final int LEFT = 3;
    public static final int DOWN = 4;

    private int current_x;
    private int current_y;

    public Position(final int positionX, final int positionY) {
        this.current_x = positionX;
        this.current_y = positionY;
    }

    public int getX() {
        return current_x;
    }

    public int getY() {
        return current_y;
    }

    public void moveRight() {
        this.current_x++;
    }

    public void moveLeft() {
        this.current_x--;
    }

    public void moveUp() {
        this.current_y--;
    }

    public void moveDown() {
        this.current_y++;
    }
}
