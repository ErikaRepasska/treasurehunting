package com.erika.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Tato trieda predsavuje populaciu.
 * Pre kazdu populaciu si drzi zoznam jedincov - hunters.
 * Jej metody zabezpecuju vytvorenie jedincov prvej generacie, ziskanie zoznamu jedincov,
 * vyber najlepsieho jedinca, sumu fitness hodnot, pridanie ci odstanenie jedinca z populacie
 * a vytvorenie nahodneho genomu.
 **/

public class Population {

    private final Random random = new Random();
    private final List<Hunter> hunters = new ArrayList<>();

    public Population() {
    }

    public void createFirstGenerationHunters(final int size) {
        for (int i = 0; i < size; i++) {
            hunters.add(new Hunter(getRandomGenome()));
        }
    }

    public List<Hunter> getHunters() {
        return hunters;
    }

    public Hunter getFittest() {
        Hunter bestHunter = hunters.get(0);
        for (final Hunter hunter : hunters) {
            if (hunter.getFitnessValue() > bestHunter.getFitnessValue()) {
                bestHunter = hunter;
            }
        }
        return bestHunter;
    }

    public int getSumOfFitnessValue() {
        int sumOfFitnessValues = 0;
        for (final Hunter hunter : hunters) {
            sumOfFitnessValues += hunter.getFitnessValue() + 1000; //posun fitness hodnot, aby ruleta pracovala s kladnymi cislami
        }

        return (sumOfFitnessValues != 0) ? sumOfFitnessValues : 1;
    }

    public void addHunter(final Hunter hunter) {
        hunters.add(hunter);
    }

    public void removeHunter(final Hunter hunter) {
        hunters.remove(hunter);
    }

    private int[] getRandomGenome() {
        final int[] genomeArray = new int[64];
        for (int i = 0; i < 16; i++) {
            final int num = random.nextInt(255 - 0 + 1) + 0;
            genomeArray[i] = num;
        }

        return genomeArray;
    }
}
