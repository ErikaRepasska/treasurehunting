package com.erika.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Trieda reprezentujuca jedinca v populacii.
 **/

public class Hunter {

    private int fitnessValue;
    private int numberOfFoundTreasures;
    private double rouletteProbability;

    private final Position currentPosition;
    private final int[] genome;
    private final List<Character> crossedPath = new ArrayList<>();

    public Hunter() {
        this(new int[64]);
    }

    public Hunter(final int[] originalGenome) {
        this.currentPosition = new Position(3, 6);
        this.genome = originalGenome;
    }

    //zistenie aktualnej pozicie jedinca na mape
    public Position getCurrentPosition() {
        return currentPosition;
    }

    public int getNumberOfFoundTreasures() {
        return numberOfFoundTreasures;
    }

    public void setNumberOfFoundTreasures(final int numberOfFoundTreasures) {
        this.numberOfFoundTreasures = numberOfFoundTreasures;
    }

    public int getFitnessValue() {
        return fitnessValue;
    }

    public void setFitnessValue(final int fitnessValue) {
        this.fitnessValue = fitnessValue;
    }

    public int[] getGenome() {
        return genome;
    }

    //ziskanie kopie genomu - aby sa nemodifikoval ten originalny
    public int[] getCopyOfGenome() {
        return Arrays.copyOf(genome, genome.length);
    }

    //posun jedinca na mape
    public boolean move(final int direction) {
        switch (direction) {
            case Position.RIGHT:
                currentPosition.moveRight();
                if (isOnTheMap()) {
                    crossedPath.add('P');
                    return true;
                } else
                    return false;
            case Position.UP:
                currentPosition.moveUp();
                if (isOnTheMap()) {
                    crossedPath.add('H');
                    return true;
                } else
                    return false;
            case Position.LEFT:
                currentPosition.moveLeft();
                if (isOnTheMap()) {
                    crossedPath.add('L');
                    return true;
                } else
                    return false;
            case Position.DOWN:
                currentPosition.moveDown();
                if (isOnTheMap()) {
                    crossedPath.add('D');
                    return true;
                } else
                    return false;
            case Position.INVALID_DIRECTION:
                return false;
            default:
                return false;
        }
    }

    //metoda zistujuca, ci sa jedinec nachadza na mape
    public boolean isOnTheMap() {
        if(currentPosition.getX() <= 6
                && currentPosition.getY() >= 0
                && currentPosition.getX() >= 0
                && currentPosition.getY() <= 6)
            return true;
        else
            return false;
    }

    //nastavenie hodnoty genu v genome na konkretnom indexe genomu
    public void setGeneOnIndex(final int index, final int value) {
        genome[index] = value;
    }

    //ziskanie hodnoty genu v genome na konkretnom indexe genomu
    public int getGeneOnIndex(final int index) {
        return genome[index];
    }

    //zoznam posunov, ktore jedinec zrealizoval na mape
    public List<Character> getCrossedPath() {
        return crossedPath;
    }

    //pravdepodobnost vyberu jedinca v rulete
    public double getRouletteProbability() {
        return rouletteProbability;
    }

    //nastavenie pravdepodobnosti jedinca do rulety
    public void setRouletteProbability(final double rouletteProbability) {
        this.rouletteProbability = rouletteProbability;
    }
}
